package com.movies.showmymovies.http;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
/**
 * Test error scenarios using a Client
 * @author sheetal
 *
 */
public class TestMovieResource {
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "showmymovies/rest/movies";
	private static final String URI_PATH_ONE = "showmymovies/rest/movies/1";
	
	private Client client = ClientBuilder.newClient();
	private WebTarget target;
	
	@Before
	public void init(){
		target = client.target(HTTP_HOST).path(URI_PATH);
	}

	@Test
	public void testGetMoviesNoParamsJson(){						
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).get();

		//you can use this to print the response
		System.out.println("HTTP Status=" + response.getStatus());
		//NOTE - you can read the entity ONLY once
		System.out.println(response.readEntity(String.class));
				
//		verifyMissing(response);
	}
	
	@Test
	public void testGetMoviesNoParamsXml(){						
		Response response =	target.request().accept(MediaType.APPLICATION_XML).get();

		//you can use this to print the response
		System.out.println("HTTP Status=" + response.getStatus());
		//NOTE - you can read the entity ONLY once
		System.out.println(response.readEntity(String.class));
//		verifyMissing(response);
	}
	
	
	@Test
	public void testGetOneMovieNoParamsXml(){
		target = client.target(HTTP_HOST).path(URI_PATH_ONE);
		Response response =	target.request().accept(MediaType.APPLICATION_XML).get();

		//you can use this to print the response
		System.out.println("HTTP Status=" + response.getStatus());
		//NOTE - you can read the entity ONLY once
		System.out.println(response.readEntity(String.class));
		Assert.assertEquals(200, response.getStatus());
	}
	
	
	@Test
	public void testGetOneMovieNoParamsJson(){				
		target = client.target(HTTP_HOST).path(URI_PATH_ONE);
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).get();

		//you can use this to print the response
		System.out.println("HTTP Status=" + response.getStatus());
		//NOTE - you can read the entity ONLY once
		System.out.println(response.readEntity(String.class));
				
		Assert.assertEquals(200, response.getStatus());
	}
	
	@Test
	public void testAutoSuggestMoviesNoParamsJson(){				
		target = client.target(HTTP_HOST).path(URI_PATH).queryParam("name", "zoo");
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).get();

		//you can use this to print the response
		System.out.println("HTTP Status=" + response.getStatus());
		//NOTE - you can read the entity ONLY once
		System.out.println(response.readEntity(String.class));
				
		Assert.assertEquals(200, response.getStatus());
	}
	
	@Test
	public void testGetAutoSuggestMovieNoParamsXml(){
		target = client.target(HTTP_HOST).path(URI_PATH).queryParam("name", "zoo");
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).get();

		//you can use this to print the response
		System.out.println("HTTP Status=" + response.getStatus());
		//NOTE - you can read the entity ONLY once
		System.out.println(response.readEntity(String.class));
		Assert.assertEquals(200, response.getStatus());
	}
//
//	private void verifyMissing(Response response) {
//		HttpError error = response.readEntity(HttpError.class);
//		Assert.assertEquals(409, response.getStatus());
//		Assert.assertEquals(409, error.status);
//		Assert.assertEquals("MISSING_DATA", error.code);
//		Assert.assertEquals("no search parameter provided", error.message);
//		Assert.assertEquals("", error.debug);		
//	}
//	
//	@Test
//	public void testCreateUsersNoParamsXml(){					
//		Response response =	target.request().accept(MediaType.APPLICATION_XML).post(Entity.entity("<user/>", MediaType.APPLICATION_XML));
//		
//		verifyInvalid(response);
//	}
//	
//	@Test
//	public void testCreateUsersNoParamsEntityXml(){					
//		HttpUser userToSend = new HttpUser();		
//		Response response =	target.request().accept(MediaType.APPLICATION_XML).post(Entity.entity(userToSend, MediaType.APPLICATION_XML));
//		
//		verifyInvalid(response);
//	}
//	
//	@Test
//	public void testCreateUsersNoParamsJson(){					
//		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity("{user:{}}", MediaType.APPLICATION_JSON));
//		
//		verifyInvalid(response);
//	}
//	
//	@Test
//	public void testCreateUsersNoParamsEntityJson(){					
//		HttpUser userToSend = new HttpUser();		
//		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(userToSend, MediaType.APPLICATION_JSON));
//		
//		verifyInvalid(response);
//	}
//
//	private void verifyInvalid(Response response) {
//		HttpError error = response.readEntity(HttpError.class);
//		Assert.assertEquals(409, response.getStatus());
//		Assert.assertEquals(409, error.status);
//		Assert.assertEquals("INVALID_FIELD", error.code);
//		Assert.assertEquals("pin is required", error.message);
//		Assert.assertEquals("", error.debug);		
//	}
//	
//	@Test
//	public void testCreateAndGetUser(){					
//		HttpUser userToSend = new HttpUser();
//		userToSend.firstName="foo"+new Random().nextInt(99999);
//		userToSend.lastName="bar"+new Random().nextInt(99999);;
//		userToSend.pin="12345";
//		
//		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(userToSend, MediaType.APPLICATION_JSON));
//		
//		HttpUser createResponse = response.readEntity(HttpUser.class);
//		//System.err.println(createResponse);
//		Assert.assertEquals(201, response.getStatus());
//		Assert.assertEquals(createResponse.firstName, userToSend.firstName);
//		Assert.assertEquals(createResponse.lastName, userToSend.lastName);
//		Assert.assertNotNull(createResponse.id);
//		Assert.assertNull(createResponse.pin);
//		
//		//search for just created user		
//		Response search = target.queryParam("firstName", userToSend.firstName).queryParam("lastName", userToSend.lastName).request().accept(MediaType.APPLICATION_JSON).get();
//		List<HttpUser> searchResponse = search.readEntity(new GenericType<List<HttpUser>>(){});
//		Assert.assertEquals(1,searchResponse.size());
//		Assert.assertEquals(searchResponse.get(0), createResponse);		
//	}
}
